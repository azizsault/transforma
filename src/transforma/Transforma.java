/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transforma;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author koonjshah
 */
public class Transforma {

    private static final Logger logger = Logger.getGlobal();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        
        
        try {
            FileHandler handler = new FileHandler("transforma.log",true);
            logger.addHandler(handler);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        HashMap<String, Object> objetos = new HashMap<>();

        for (String parameter : args) {
            transformar(parameter, objetos);
        }

        try {
            if (objetos.containsKey("arquivo")) {
                Transforma.create(objetos);
            } else {
                throw new IllegalArgumentException("Não foi passado o arquivo .jasper no parâmetro do transforma.");
            }
        } catch (IllegalArgumentException ex) {
            final String message = ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            final String message = "Erro desconhecido: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

    }
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

    private static void transformar(String parameter, HashMap<String, Object> objetos) {
        String[] splitedParam = parameter.split("=");
        
        String key = splitedParam[0];
        System.out.println("Buscando a chave: "+ key);
        
        
        try{
            String value = splitedParam[1];
            System.out.println("cahve:" + key + " valor: "+ value);
        
            if(key.endsWith("dateobj")){
                try {
                    Date dt = sdf.parse(value);
                    objetos.put(key, dt);
                } catch (ParseException ex) {
                    objetos.put(key, value);
                }
            } else {
                objetos.put(key, value);
            }
        } catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("Descartando :"+ key);
            //não faço nada.
        }
    }

    public static void create(Map<String, Object> objetos) {
        Transforma t = new Transforma();
        t.simpleReport(objetos);
    }

    public void simpleReport(Map<String, Object> objetos) {
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(objetos.remove("arquivo").toString(), objetos, getConnection());
            JasperViewer jasperViewer = new JasperViewer(jasperPrint);
            jasperViewer.setVisible(true);

        } catch (JRException ex) {
            if (ex.getCause() instanceof FileNotFoundException) {
                final String message = "Não foi possível encontrar o arquivo: " + ex.getMessage();
                logger.log(Level.SEVERE, message, ex);
                JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
            } else {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
                JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        } catch (java.lang.NoClassDefFoundError ex) {
            final String message = "Classe: " + ex.getMessage() + " não encontrada.";
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException ex) {
            final String message = ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            final String message = "Erro desconhecido: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

    }

    private Connection getConnection() {
        Connection con = null;
        try {
            Class.forName(Settings.DRIVER.getValue());
            con = DriverManager.getConnection(Settings.URL.getValue(), Settings.USER.getValue(), Settings.PASSWORD.getValue());
        } catch (ClassNotFoundException | SQLException e) {
            final String message = "Erro ao se conectar";
            logger.log(Level.SEVERE, message, e);
            JOptionPane.showMessageDialog(null, message, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return con;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package transforma;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.tools.shell.Main;

/**
 *
 * @author koonjshah
 */
public enum Settings {
    DRIVER,
    URL,
    USER,
    PASSWORD;

    private static final String PATH            = "bd.properties";

    private static final Logger logger          = Logger.getGlobal();

    private final static Properties   properties;

    static{

        try {
            FileHandler handler = new FileHandler("driver.log",true);
            logger.addHandler(handler);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        properties = new Properties();
        try {
            String caminho = Settings.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("Transforma.jar", "");
            System.out.println("Arquivo bd.properties: "+ caminho);
            System.out.println("");
            
            InputStream file = new FileInputStream(caminho + PATH);
            properties.load(file);
        } catch (Exception e) {
            final String message = "Não foi possível carregar o arquivo: " + PATH + ".";
            logger.log(Level.SEVERE, message, e);
            throw new IllegalArgumentException(message);
        }
    }


    public String getValue() {
        return (String) properties.get(this.name().toLowerCase());
    }

}
